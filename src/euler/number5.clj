(ns euler.number5)

(defn divides? [x i]
  (zero? (mod x i)))

(defn divided-by-1-to-n? [x n]
  (every? #(divides? x %) (range 1 (inc n))))

(defn smallest-divided-by-1-to-n [n]
  (inc (last (take-while #(not (divided-by-1-to-n? % n)) (range 1 java.lang.Integer/MAX_VALUE n)))))

(smallest-divided-by-1-to-n 20)
