(ns euler.number29
  (:require [clojure.math.numeric-tower :as math]))

(count (set (for [x (range 2 101) y (range 2 101)] (math/expt x y))))


