(ns euler.number9)

(defn squared? [x]
  (let [s (Math/sqrt x)]
  (= s (Math/floor s))))

(defn hypothenuse [a b]
  (Math/sqrt (+ (Math/pow a 2) (Math/pow b 2))))

(defn pythagorean? [[a b & args]]
  (squared? (+ (Math/pow a 2) (Math/pow b 2))))

(defn pythagorean-seq [n]
  (map #(map int %) (filter pythagorean? (for [a (range 1 n) b (range a n)] [a b (hypothenuse a b)]))))

(defn find-pythagorean-with-sum [x, maximum]
  (filter #(= x (reduce + %)) (pythagorean-seq maximum)))

(reduce * (first (find-pythagorean-with-sum 1000 1000)))

