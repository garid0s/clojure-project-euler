(ns euler.number3)

(defn divides? [x i]
  (zero? (mod x i)))

(defn factors [x]
    (flatten (map #(list %, (/ x %)) (filter #(divides? x %) (range 1 (inc (Math/floor (Math/sqrt x))))))))

(defn prime? [x]
  memoize (empty? (filter #(and divides? (not= x %) (not= 1 %)) (factors x))))

(defn prime-factors [x]
  (filter #(and (prime? %) (not= 1 %)) (factors x)))

(last (prime-factors 600851475143))
