(ns euler.number1)

(defn multiple-of-3-or-5? [x]
  (cond
    (zero? (mod x 3)) true
    (zero? (mod x 5)) true
    :else false))

(defn sum-of-multiples-3-and-5 [n]
  "Sum multiples of 3 and 5 from 1 to n - 1"
  (reduce + (filter multiple-of-3-or-5? (range 1  n))))

(sum-of-multiples-3-and-5 1000)
