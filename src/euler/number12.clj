(ns euler.number12)

(defn divides? [x i]
  (zero? (mod x i)))

(defn factors [x]
    (set (flatten (map #(list %, (/ x %)) (filter #(divides? x %) (range 1 (inc (Math/floor (Math/sqrt x)))))))))

(def triangular-num
  (for [x (range 1 java.lang.Integer/MAX_VALUE)] (reduce + (range (inc x)))))

(defn triangular-with-more-than-n-factors [n]
  (filter #(> (count (factors %)) n) triangular-num))

(first (triangular-with-more-than-n-factors 500))
