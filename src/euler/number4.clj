(ns euler.number4)
(require ['clojure.string :as 'string])

(defn is-palindrome? [s]
  (= s (string/reverse s)))

(defn largest-palindrome-product-n-digits [n]
  (apply max (filter #(is-palindrome? (str %)) (all-products-of-number-n-digits n))))

(defn all-products-of-number-n-digits [n]
  (distinct (for [x (range (Math/pow 10 n)) y (range (Math/pow 10 n))] (* x y))))

(largest-palindrome-product-n-digits 3)
