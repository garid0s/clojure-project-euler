(ns euler.number26)

(defn lenght-of-recuring-part [x]
  (loop [num 10 seen-nums [] result []]
    (cond 
      (= num 0) 0 
      (not (nil? (some #{num} seen-nums))) (- (count seen-nums) (.indexOf seen-nums num))
      (< num x) (recur (* num 10) (conj seen-nums num) (conj result 0))
      :else (let [div-result (quot num x)]
              (recur (* 10 (mod num x)) (conj seen-nums num) (conj result div-result)))))) 

(loop [x 1 max-recuring 0 best-x 0]
  (if (< x 1000)
    (let [recuring-part-of-x (lenght-of-recuring-part x)]
    (if (> recuring-part-of-x max-recuring) 
      (recur (inc x) recuring-part-of-x x)
      (recur (inc x) max-recuring best-x)))
      [max-recuring best-x]))

(lenght-of-recuring-part 65)
