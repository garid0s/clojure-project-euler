(ns euler.number15)

(defn fact [x]
  (loop [n x f 1]
    (if (= n 1)
      f
      (recur (dec n) (* f n)))))

(/ (fact 40N) (* (fact 20N) (fact 20N)))
