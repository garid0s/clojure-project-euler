(ns euler.number31
  (:require [clojure.math.numeric-tower :as math]))

(def pieces '(1 2 5 10 20 50 100 200))

(def iter-seq (for [p pieces m (range 201)]
  [m p]))

(defn piece-prec [p]
  (if (= 1 p)
    0
    (loop [r pieces]
      (if (= p (second r))
        (first r)
        (recur (rest r))))))

(defn multiples-of-x-below-or-equal-y [x y]
  (map #(* x %) (range (inc (quot y x)))))

(defn value-of-cell [[m p] table]
  (if (or (= m 0) (= p 1)) 
    1
    (loop [to-sum [] iter (multiples-of-x-below-or-equal-y p m)]
      (if (nil? (first iter))
        (apply + to-sum)
        (let [n (first iter) p-prec (piece-prec p)]
          (let [cell (get table [(- m n) p-prec])]
            (if (nil? cell)
              (recur to-sum (rest iter))
              (recur (conj to-sum cell) (rest iter)))))))))

(loop [table {} iter iter-seq]
  (let [i (first iter)]
    (if (nil? i)
      (get table [200 200])
      (let [[m p] i]
        (recur (conj table [[m p] (value-of-cell [m p] table)]) (rest iter))))))
