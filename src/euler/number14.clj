(ns euler.number14)

(defn collatz-seq [x]
  (if (and (not= 1 x) (not= 0 x))
    (if (even? x) 
      (cons x (lazy-seq (collatz-seq (/ x 2))))
      (cons x (lazy-seq (collatz-seq (+ 1 (* 3 x))))))
    [x]))

(defn length-of-collatz [x]
  (count (collatz-seq x)))

(defn all-start-and-length [x]
  (for [start (range x)]
    (let [length (length-of-collatz start)]
      [start length])))

(defn longest-collatz-starting-under [x]
  (let [col (all-start-and-length x)]
    (first (apply max-key second col))))
  

(time (longest-collatz-starting-under 1000000))
