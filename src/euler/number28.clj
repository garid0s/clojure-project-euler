(ns euler.number28)

(defn sum-of-spiral-diago [size]
  (loop [sum 1 num 3 i 1 circle-size 3]
    (cond (= i 4)               (recur (+ sum num) (+ num circle-size 1)     1       (+ circle-size 2))
          (<= circle-size size) (recur (+ sum num) (+ num (- circle-size 1)) (inc i) circle-size)
          :else sum)
      ))

(sum-of-spiral-diago 1001)
