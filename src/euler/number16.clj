(ns euler.number16)


(reduce + (map #(Character/getNumericValue %) (str (.pow (java.math.BigInteger/valueOf 2)1000))))
