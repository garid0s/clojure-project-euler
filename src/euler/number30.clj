(ns euler.number30
  (:require [clojure.math.numeric-tower :as math]))

(defn number-as-seq-of-digit [n]
  (map (fn [^Character c] (Character/digit c 10)) (str n)))

(apply + (loop [num-list [] i 10]
  (cond 
    (= i (apply + (map #(math/expt % 5)(number-as-seq-of-digit i)))) (recur (conj num-list i) (inc i))
    (> i 999999) num-list
    :else (recur num-list (inc i)))))
