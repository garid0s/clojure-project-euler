(ns euler.number27)

(defn divides? [x i]
  (zero? (mod x i)))

(defn factors [x]
    (flatten (map #(list % (/ x %)) (filter #(divides? x %) (range 1 (inc (Math/floor (Math/sqrt x))))))))

(defn prime? [x]
  (empty? (filter #(and divides? (not= x %) (not= 1 %)) (factors x))))

(def primes 
  (filter prime? (range 2 java.lang.Integer/MAX_VALUE)))

(defn poly [a b]
  (fn [n] (+ (* n n) (* n a) b)))

(defn number-of-primes-from-n-0 [a b]
  (let [p (poly a b)]
  (loop [n 0]
    (if (prime? (p n))
      (recur (inc n))
      (- n 1)))))

(for [x (range 1000) y (range 1000)] (println x y))

(number-of-primes-from-n-0 1 41)


