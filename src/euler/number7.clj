(ns euler.number7)

(defn divides? [x i]
  (zero? (mod x i)))

(defn factors [x]
    (flatten (map #(list %, (/ x %)) (filter #(divides? x %) (range 1 (inc (Math/floor (Math/sqrt x))))))))

(defn prime? [x]
  (empty? (filter #(and divides? (not= x %) (not= 1 %)) (factors x))))

(def primes 
  (filter prime? (range 2 java.lang.Integer/MAX_VALUE)))

(nth primes 10000)
