(ns euler.number2)

(def fib-seq 
  ((fn rfib [a b] 
     (lazy-seq (cons a (rfib b (+ a b)))))
   0 1))

(reduce + (filter even? (take-while #(<= % 4000000) fib-seq)))
